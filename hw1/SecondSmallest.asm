.data
values:  .word   13, 16, 16, 7, 7	#Array contents
size:    .word   5			        #Size of the array
minimum: .word   0			        #Minimum Number
counter: .word   0		        	#array Counter
printnot:    .asciiz                "There is No Second Smallest"
printmin:    .asciiz                "Second Smallest Number : "

.text

la      $s0, values                 # s0 = values
lw      $s1, size		            # s1 = size
la      $s2, minimum                # s2 = minimum
lw	    $s3, counter		        # s3 =Counter
lw      $t4, 0($s0)                 # t4 = values[0] assign
sw      $t4, 0($s2)                 # assign minimum value

Loop:
    beq     $s1,$s3,end             #Loop (Size=Counter) End
    lw      $t0,0($s0)              # $t0 = *values
    addi    $s0,$s0,4               # values++
    addi    $s3,$s3,1               # counter++
    lw      $t1,0($s2)              # assign minimum value
    slt     $t2,$t1,$t0             # *values < min?
    bne     $t2,$zero,Loop          # no, loop
    lw      $t5,0($s2)
    beq     $t0,$t5,label1		    # If t0 equal t5 go label1
    sw      $t5,4($s2)
    j       Loop
label1:
    sw      $t0,0($s2)              # assign new minimum
    j       Loop
end:
    lw      $t0,4($s2)              
    lw      $t1,0($s2)                 
    beq     $t0,$zero,label2        #If t0 equal 0 label2  and print the message.
    li      $v0,4
    la      $a0,printmin	        #print "Second Smallest Number :"
    syscall
    li      $v0,1
    lw      $a0,4($s2)              #get minimum
    syscall
    
li          $v0,10			        #Exit all system...
syscall
    
label2:
    li      $v0,4
    la      $a0,printnot	        #print "There is No Second Smallest"
    syscall
    li      $v0,10
    syscall